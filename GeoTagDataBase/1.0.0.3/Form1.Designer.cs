﻿namespace GeoTagDataBase
{
    partial class FormDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
            this.sqlCommand1 = new System.Data.SqlClient.SqlCommand();
            this.buttonExecuteQuery = new System.Windows.Forms.Button();
            this.bindingSourceGeoRiotinto = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxLABELNR = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelRowCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelNotVerifiedCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonRewrite = new System.Windows.Forms.Button();
            this.checkBoxPassOnly = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceGeoRiotinto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sqlConnection1
            // 
            this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;
            // 
            // sqlCommand1
            // 
            this.sqlCommand1.Connection = this.sqlConnection1;
            // 
            // buttonExecuteQuery
            // 
            this.buttonExecuteQuery.Location = new System.Drawing.Point(194, 16);
            this.buttonExecuteQuery.Name = "buttonExecuteQuery";
            this.buttonExecuteQuery.Size = new System.Drawing.Size(75, 23);
            this.buttonExecuteQuery.TabIndex = 0;
            this.buttonExecuteQuery.Text = "&Query";
            this.buttonExecuteQuery.UseVisualStyleBackColor = true;
            this.buttonExecuteQuery.Click += new System.EventHandler(this.buttonExecuteQuery_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.DataSource = this.bindingSourceGeoRiotinto;
            this.dataGridView1.Location = new System.Drawing.Point(12, 45);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(588, 416);
            this.dataGridView1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "LABELNR:";
            // 
            // textBoxLABELNR
            // 
            this.textBoxLABELNR.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::GeoTagDataBase.Properties.Settings.Default, "LABELNR", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxLABELNR.Location = new System.Drawing.Point(78, 18);
            this.textBoxLABELNR.MaxLength = 7;
            this.textBoxLABELNR.Name = "textBoxLABELNR";
            this.textBoxLABELNR.Size = new System.Drawing.Size(110, 20);
            this.textBoxLABELNR.TabIndex = 3;
            this.textBoxLABELNR.Text = global::GeoTagDataBase.Properties.Settings.Default.LABELNR;
            this.textBoxLABELNR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxLABELNR_KeyPress);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelRowCount,
            this.toolStripStatusLabelNotVerifiedCount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 464);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(612, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelRowCount
            // 
            this.toolStripStatusLabelRowCount.Name = "toolStripStatusLabelRowCount";
            this.toolStripStatusLabelRowCount.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabelNotVerifiedCount
            // 
            this.toolStripStatusLabelNotVerifiedCount.Name = "toolStripStatusLabelNotVerifiedCount";
            this.toolStripStatusLabelNotVerifiedCount.Size = new System.Drawing.Size(0, 17);
            // 
            // buttonRewrite
            // 
            this.buttonRewrite.Location = new System.Drawing.Point(275, 16);
            this.buttonRewrite.Name = "buttonRewrite";
            this.buttonRewrite.Size = new System.Drawing.Size(75, 23);
            this.buttonRewrite.TabIndex = 5;
            this.buttonRewrite.Text = "&Rewrite";
            this.buttonRewrite.UseVisualStyleBackColor = true;
            this.buttonRewrite.Click += new System.EventHandler(this.buttonRewrite_Click);
            // 
            // checkBoxPassOnly
            // 
            this.checkBoxPassOnly.AutoSize = true;
            this.checkBoxPassOnly.Checked = global::GeoTagDataBase.Properties.Settings.Default.passOnly;
            this.checkBoxPassOnly.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::GeoTagDataBase.Properties.Settings.Default, "passOnly", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBoxPassOnly.Location = new System.Drawing.Point(524, 20);
            this.checkBoxPassOnly.Name = "checkBoxPassOnly";
            this.checkBoxPassOnly.Size = new System.Drawing.Size(76, 17);
            this.checkBoxPassOnly.TabIndex = 6;
            this.checkBoxPassOnly.Text = "&PASS only";
            this.checkBoxPassOnly.UseVisualStyleBackColor = true;
            this.checkBoxPassOnly.CheckedChanged += new System.EventHandler(this.checkBoxPassOnly_CheckedChanged);
            // 
            // FormDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 486);
            this.Controls.Add(this.checkBoxPassOnly);
            this.Controls.Add(this.buttonRewrite);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.textBoxLABELNR);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buttonExecuteQuery);
            this.Name = "FormDB";
            this.Text = "Geo Tag Database";
            this.Load += new System.EventHandler(this.FormDB_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormDB_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceGeoRiotinto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Data.SqlClient.SqlConnection sqlConnection1;
        private System.Data.SqlClient.SqlCommand sqlCommand1;
        private System.Windows.Forms.Button buttonExecuteQuery;
        private System.Windows.Forms.BindingSource bindingSourceGeoRiotinto;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLABELNR;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRowCount;
        private System.Windows.Forms.Button buttonRewrite;
        private System.Windows.Forms.CheckBox checkBoxPassOnly;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelNotVerifiedCount;
    }
}

