﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GeoTagDataBase
{
    public partial class FormDB : Form
    {
        enum TRACKING_STATE
        {
            Ready,
            Verify,
            Pass,
            Fail
        }

        private const string GEO_CONN_STRING = "Data Source=ERPTEST;Initial Catalog=GeoRiotinto;User ID=rfid;Password=rfid";
        private const string SELECTED_COLUMN = "[TIMESTMP], [WONR], [LABELNR], [PASS], [ERRORCODE], [VERIFIED]";
        private SqlDataAdapter dataAdapter = new SqlDataAdapter();

        public FormDB()
        {
            InitializeComponent();
            this.Text += " (" + Application.ProductVersion + ")";
        }

        private void FormDB_Load(object sender, EventArgs e)
        {
            this.sqlConnection1.ConnectionString = GEO_CONN_STRING;
            queryTagInfo();
            this.textBoxLABELNR.Select();
        }

        private void buttonExecuteQuery_Click(object sender, EventArgs e)
        {
            queryTagInfo();
        }
        private void textBoxLABELNR_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x0D)
            {
                queryTagInfo();
            }
        }
        private void queryTagInfo()
        {
            if (this.textBoxLABELNR.Text.Length > 3)
            {
                string selectCommand = "";
                if (checkBoxPassOnly.Checked)
                {
                    selectCommand = "SELECT " + SELECTED_COLUMN + " from TAGSREAD "
                        + "WHERE LABELNR LIKE \'" + this.textBoxLABELNR.Text + "%\' "
                        + "AND [PASS] = 'PASS' "
                        + "ORDER BY [LABELNR], [TIMESTMP]";
                }
                else
                {
                    selectCommand = "SELECT " + SELECTED_COLUMN + " from TAGSREAD "
                        + "WHERE LABELNR LIKE \'" + this.textBoxLABELNR.Text + "%\' "
                        + "ORDER BY [LABELNR], [TIMESTMP]";
                }
                GetData(selectCommand);
                toolStripStatusLabelRowCount.Text = "Row count = " + dataGridView1.RowCount;
                toolStripStatusLabelNotVerifiedCount.Text = " Not verified count = " + calNotVerified();
            }
            else
            {
                MessageBox.Show("Please type label number at least 4 characters", "Input Error");
            }
        }
        private string calNotVerified()
        {
            int verifiedCount = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                Object ob = dataGridView1.Rows[i].Cells[5].Value;
                verifiedCount += Convert.ToInt16(ob.ToString());
            }
            return (dataGridView1.Rows.Count - verifiedCount).ToString();
        }
        private void GetData(string selectCommand)
        {
            try
            {
                // Specify a connection string. Replace the given value with a 
                // valid connection string for a Northwind SQL Server sample
                // database accessible to your system.
                //String connectionString = this.sqlConnection1.ConnectionString;
                String connectionString = GEO_CONN_STRING;

                // Create a new data adapter based on the specified query.
                dataAdapter = new SqlDataAdapter(selectCommand, connectionString);

                // Create a command builder to generate SQL update, insert, and
                // delete commands based on selectCommand. These are used to
                // update the database.
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                // Populate a new data table and bind it to the BindingSource.
                DataTable table = new DataTable();
                //table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSourceGeoRiotinto.DataSource = table;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString(), "SQL Error");
            }
        }

        private void buttonRewrite_Click(object sender, EventArgs e)
        {
            if (this.textBoxLABELNR.Text.Length == 7)
            {
                GetData("SELECT " + SELECTED_COLUMN + " from TAGSREAD WHERE LABELNR = \'" + this.textBoxLABELNR.Text + "\'");
                if (dataGridView1.RowCount < 1)
                {
                    MessageBox.Show("There is no this LABELNR", "LABELNR Error");
                    return;
                }

                DialogResult dResult = new FormPassword().ShowDialog();
                if (dResult == DialogResult.OK)
                {
                    RewriteData(this.textBoxLABELNR.Text);
                    queryTagInfo();
                }
            }
            else
            {
                MessageBox.Show("Please type label number 7 characters", "Input Error");
            }
        }
        private void RewriteData(string _LABELNR)
        {
            string updateCommand = "UPDATE [GeoRiotinto].[dbo].[TAGSREAD] SET [LABELNR]=\'"
                + _LABELNR + "-f\', [PASS]=\'FAIL\', [ERRORCODE] = 13 WHERE [LABELNR]=\'" + _LABELNR + "\'";
            SqlCommand _sqlCommand = new SqlCommand(updateCommand, this.sqlConnection1);

            this.sqlConnection1.Open();
            int i = _sqlCommand.ExecuteNonQuery();
            this.sqlConnection1.Close();
        }

        private void FormDB_FormClosed(object sender, FormClosedEventArgs e)
        {
            global::GeoTagDataBase.Properties.Settings.Default.Save();
        }

        private void checkBoxPassOnly_CheckedChanged(object sender, EventArgs e)
        {
            queryTagInfo();
        }

    }
}
