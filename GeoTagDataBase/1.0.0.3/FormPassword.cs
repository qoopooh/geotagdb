﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GeoTagDataBase
{
    public partial class FormPassword : Form
    {
        private const string password = "aae_123";
        public FormPassword()
        {
            InitializeComponent();
        }

        private void FormPassword_Load(object sender, EventArgs e)
        {
            this.maskedTextBox1.Select();
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x0D)
            {
                checkPassword();
            }
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            checkPassword();
        }
        private void checkPassword()
        {
            if (maskedTextBox1.Text == password)
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Please re-enter password", "Incorrect password");
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
