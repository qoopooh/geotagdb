﻿namespace GeoTagDataBase
{
    partial class FormDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDB));
            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
            this.sqlCommand1 = new System.Data.SqlClient.SqlCommand();
            this.buttonExecuteQuery = new System.Windows.Forms.Button();
            this.bindingSourceGeoRiotinto = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.labelSelectedVerifyingIndex = new System.Windows.Forms.Label();
            this.labelRowCount = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelNotVerifiedCount = new System.Windows.Forms.Label();
            this.panelStatus = new System.Windows.Forms.Panel();
            this.labelStatus = new System.Windows.Forms.Label();
            this.checkBoxVerifying = new System.Windows.Forms.CheckBox();
            this.numericUpDownWONR = new System.Windows.Forms.NumericUpDown();
            this.timerQuery = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceGeoRiotinto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panelStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWONR)).BeginInit();
            this.SuspendLayout();
            // 
            // sqlConnection1
            // 
            this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;
            // 
            // sqlCommand1
            // 
            this.sqlCommand1.Connection = this.sqlConnection1;
            // 
            // buttonExecuteQuery
            // 
            this.buttonExecuteQuery.Location = new System.Drawing.Point(15, 29);
            this.buttonExecuteQuery.Name = "buttonExecuteQuery";
            this.buttonExecuteQuery.Size = new System.Drawing.Size(75, 23);
            this.buttonExecuteQuery.TabIndex = 0;
            this.buttonExecuteQuery.Text = "&Query";
            this.buttonExecuteQuery.UseVisualStyleBackColor = true;
            this.buttonExecuteQuery.Click += new System.EventHandler(this.buttonExecuteQuery_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.DataSource = this.bindingSourceGeoRiotinto;
            this.dataGridView1.Location = new System.Drawing.Point(12, 58);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(295, 189);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dataGridView1_Scroll);
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseClick);
            this.dataGridView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridView1_KeyPress);
            // 
            // labelSelectedVerifyingIndex
            // 
            this.labelSelectedVerifyingIndex.AutoSize = true;
            this.labelSelectedVerifyingIndex.Location = new System.Drawing.Point(144, 9);
            this.labelSelectedVerifyingIndex.Name = "labelSelectedVerifyingIndex";
            this.labelSelectedVerifyingIndex.Size = new System.Drawing.Size(59, 13);
            this.labelSelectedVerifyingIndex.TabIndex = 2;
            this.labelSelectedVerifyingIndex.Text = "LABELNR:";
            // 
            // labelRowCount
            // 
            this.labelRowCount.AutoSize = true;
            this.labelRowCount.Location = new System.Drawing.Point(209, 9);
            this.labelRowCount.Name = "labelRowCount";
            this.labelRowCount.Size = new System.Drawing.Size(35, 13);
            this.labelRowCount.TabIndex = 6;
            this.labelRowCount.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "WONR:";
            // 
            // labelNotVerifiedCount
            // 
            this.labelNotVerifiedCount.AutoSize = true;
            this.labelNotVerifiedCount.Location = new System.Drawing.Point(209, 35);
            this.labelNotVerifiedCount.Name = "labelNotVerifiedCount";
            this.labelNotVerifiedCount.Size = new System.Drawing.Size(35, 13);
            this.labelNotVerifiedCount.TabIndex = 9;
            this.labelNotVerifiedCount.Text = "label3";
            // 
            // panelStatus
            // 
            this.panelStatus.BackColor = System.Drawing.Color.Olive;
            this.panelStatus.Controls.Add(this.labelStatus);
            this.panelStatus.Location = new System.Drawing.Point(12, 253);
            this.panelStatus.Name = "panelStatus";
            this.panelStatus.Size = new System.Drawing.Size(295, 111);
            this.panelStatus.TabIndex = 11;
            // 
            // labelStatus
            // 
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labelStatus.Location = new System.Drawing.Point(3, 36);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(292, 39);
            this.labelStatus.TabIndex = 0;
            this.labelStatus.Text = "READY";
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxVerifying
            // 
            this.checkBoxVerifying.AutoSize = true;
            this.checkBoxVerifying.Checked = global::GeoTagDataBaseMonitor.Properties.Settings.Default.VerifiedFlag;
            this.checkBoxVerifying.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::GeoTagDataBaseMonitor.Properties.Settings.Default, "VerifiedFlag", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBoxVerifying.Location = new System.Drawing.Point(114, 33);
            this.checkBoxVerifying.Name = "checkBoxVerifying";
            this.checkBoxVerifying.Size = new System.Drawing.Size(66, 17);
            this.checkBoxVerifying.TabIndex = 12;
            this.checkBoxVerifying.Text = "&Verifying";
            this.checkBoxVerifying.UseVisualStyleBackColor = true;
            this.checkBoxVerifying.CheckStateChanged += new System.EventHandler(this.checkBoxVerifying_CheckStateChanged);
            // 
            // numericUpDownWONR
            // 
            this.numericUpDownWONR.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::GeoTagDataBaseMonitor.Properties.Settings.Default, "intWONR", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.numericUpDownWONR.Location = new System.Drawing.Point(63, 7);
            this.numericUpDownWONR.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDownWONR.Name = "numericUpDownWONR";
            this.numericUpDownWONR.Size = new System.Drawing.Size(75, 20);
            this.numericUpDownWONR.TabIndex = 10;
            this.numericUpDownWONR.Value = global::GeoTagDataBaseMonitor.Properties.Settings.Default.intWONR;
            this.numericUpDownWONR.ValueChanged += new System.EventHandler(this.numericUpDownWONR_ValueChanged);
            this.numericUpDownWONR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDownWONR_KeyPress);
            // 
            // timerQuery
            // 
            this.timerQuery.Enabled = global::GeoTagDataBaseMonitor.Properties.Settings.Default.VerifiedFlag;
            this.timerQuery.Interval = 1000;
            this.timerQuery.Tick += new System.EventHandler(this.timerQuery_Tick);
            // 
            // FormDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 375);
            this.Controls.Add(this.checkBoxVerifying);
            this.Controls.Add(this.panelStatus);
            this.Controls.Add(this.numericUpDownWONR);
            this.Controls.Add(this.labelNotVerifiedCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelRowCount);
            this.Controls.Add(this.labelSelectedVerifyingIndex);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buttonExecuteQuery);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormDB";
            this.Text = "Geo Tag Database Monitor";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormDB_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormDB_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceGeoRiotinto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panelStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWONR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Data.SqlClient.SqlConnection sqlConnection1;
        private System.Data.SqlClient.SqlCommand sqlCommand1;
        private System.Windows.Forms.Button buttonExecuteQuery;
        private System.Windows.Forms.BindingSource bindingSourceGeoRiotinto;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label labelSelectedVerifyingIndex;
        private System.Windows.Forms.Label labelRowCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelNotVerifiedCount;
        private System.Windows.Forms.NumericUpDown numericUpDownWONR;
        private System.Windows.Forms.Panel panelStatus;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Timer timerQuery;
        private System.Windows.Forms.CheckBox checkBoxVerifying;
    }
}

