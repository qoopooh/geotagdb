﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GeoTagDataBase
{
    public partial class FormDB : Form
    {
        enum TRACKING_STATE
        {
            Ready,
            Verify,
            Pass,
            Fail
        }

        private const string GEO_CONN_STRING = "Data Source=ERPTEST;Initial Catalog=GeoRiotinto;User ID=rfid;Password=rfid";
        private const string SELECTED_COLUMN = "[WONR], [LABELNR], [VERIFIED]";
        private SqlDataAdapter dataAdapter = new SqlDataAdapter();
        private DataTable table;
        private TRACKING_STATE trackingState;
        private TRACKING_STATE trackingPreviousState;
        private DateTime trackingStateChangeTimeStamp;
        private int selectedVerifyingRow;

        public FormDB()
        {
            InitializeComponent();
            this.Text += " (" + Application.ProductVersion + ")";
        }

        private void FormDB_Load(object sender, EventArgs e)
        {
            table = new DataTable();
            this.sqlConnection1.ConnectionString = GEO_CONN_STRING;
            setTrackingStatus(TRACKING_STATE.Verify);
            this.numericUpDownWONR.Select();
        }

        private void FormDB_FormClosed(object sender, FormClosedEventArgs e)
        {
            global::GeoTagDataBaseMonitor.Properties.Settings.Default.Save();
        }
        
        private void timerQuery_Tick(object sender, EventArgs e)
        {
            queryTagInfo();
        }
        private void queryTagInfo()
        {
            if (this.numericUpDownWONR.Value > 1000000)
            {
                string selectCommand = "SELECT " + SELECTED_COLUMN + " from TAGSREAD "
                    + "WHERE [PASS] = \'PASS\' AND [WONR] = \'"
                    + this.numericUpDownWONR.Value.ToString() + "\' ORDER BY [LABELNR]";
                GetData(selectCommand);
                labelRowCount.Text = "Row count = " + dataGridView1.RowCount;
                labelNotVerifiedCount.Text = "Not verified count = " + checkNotVerified();
                labelSelectedVerifyingIndex.Text = "Idx. " + this.selectedVerifyingRow.ToString();
                if (this.trackingState != TRACKING_STATE.Ready)
                {
                    verifyTag();
                }
            }
            else
            {
                MessageBox.Show("Please type label number 8 characters", "Input Error");

                setTrackingStatus(TRACKING_STATE.Ready);
            }
        }
        private void verifyTag()
        {
            if (dataGridView1.RowCount > this.selectedVerifyingRow)
            {
                switch (this.trackingState)
                {
                    case TRACKING_STATE.Verify:
                        //if (verifyTagBackward())
                        {
                            if (verifyTagForward())
                            {
                                this.setTrackingStatus(TRACKING_STATE.Pass);
                            }
                            else
                            {
                                this.setTrackingStatus(TRACKING_STATE.Fail);
                            }
                        }
                        //else
                        //{
                        //    this.setTrackingStatus(TRACKING_STATE.Fail);
                        //}
                        break;
                    case TRACKING_STATE.Fail:
                        if (verifyTagBackward())
                        {
                            this.setTrackingStatus(TRACKING_STATE.Pass);
                        }
                        break;
                    case TRACKING_STATE.Pass:
                        if (verifyTagForward())
                        {
                            this.setTrackingStatus(TRACKING_STATE.Pass);
                        }
                        else
                        {
                            this.setTrackingStatus(TRACKING_STATE.Fail);
                        }
                        break;
                    default:
                        break;
                }

                this.dataGridView1.CurrentCell = dataGridView1.Rows[this.selectedVerifyingRow 
                    + getMaxOffsetRow(2)].Cells[0];
                this.dataGridView1.CurrentCell = dataGridView1.Rows[this.selectedVerifyingRow].Cells[0];
            }
        }
        private int getMaxOffsetRow(int p)
        {
            int mxOffset = dataGridView1.RowCount - this.selectedVerifyingRow;
            if (p >= mxOffset) p = mxOffset-1;
            return p;
        }
        private bool verifyTagForward()
        {
            for (int i = this.selectedVerifyingRow; i < this.dataGridView1.RowCount; i++)
            {
                Object ob = dataGridView1.Rows[i].Cells["VERIFIED"].Value;
                int j = Convert.ToInt16(ob.ToString());
                if (j == 0)
                {
                    for (int k = this.selectedVerifyingRow + 1; k < this.dataGridView1.RowCount; k++)
                    {
                        ob = dataGridView1.Rows[k].Cells["VERIFIED"].Value;
                        j = Convert.ToInt16(ob.ToString());
                        if (j != 0)
                        {
                            this.selectedVerifyingRow = i;
                            return false;
                        }
                    }
                    this.selectedVerifyingRow = i - 1;
                    return true;
                }
                else
                {
                    this.selectedVerifyingRow = i;
                }
            }

            this.selectedVerifyingRow = this.dataGridView1.RowCount - 1;
            return true;
        }
        private bool verifyTagBackward()
        {
            for (int i = 0; i < this.selectedVerifyingRow + 1; i++)
            {
                Object ob = dataGridView1.Rows[i].Cells["VERIFIED"].Value;
                int j = Convert.ToInt16(ob.ToString());
                if (j == 0)
                {
                    this.selectedVerifyingRow = i;
                    return false;
                }
            }

            return true;
        }
        private void GetData(string selectCommand)
        {
            try
            {
                // Specify a connection string. Replace the given value with a 
                // valid connection string for a Northwind SQL Server sample
                // database accessible to your system.
                //String connectionString = this.sqlConnection1.ConnectionString;
                String connectionString = GEO_CONN_STRING;

                // Create a new data adapter based on the specified query.
                dataAdapter = new SqlDataAdapter(selectCommand, connectionString);

                // Create a command builder to generate SQL update, insert, and
                // delete commands based on selectCommand. These are used to
                // update the database.
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                // Populate a new data table and bind it to the BindingSource.
                //table = new DataTable();
                table.Clear();
                //table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSourceGeoRiotinto.DataSource = table;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString(), "SQL Error");
            }
        }
        private string checkNotVerified()
        {
            int verifiedCount = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                Object ob = dataGridView1.Rows[i].Cells["VERIFIED"].Value;
                verifiedCount += Convert.ToInt16(ob.ToString());
            }
            return (dataGridView1.Rows.Count - verifiedCount).ToString();
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            //setTrackingStatus(TRACKING_STATE.Ready);
        }
        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            setTrackingStatus(TRACKING_STATE.Ready);
            this.selectedVerifyingRow = GetRowIndexOfDataGridView1();
        }
        private int GetRowIndexOfDataGridView1()
        {
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (dataGridView1.Rows[i].Selected)
                {
                    return i;
                }
            }
            return dataGridView1.RowCount;
        }

        private void buttonExecuteQuery_Click(object sender, EventArgs e)
        {
            setTrackingStatus(TRACKING_STATE.Verify);
        }
        private void numericUpDownWONR_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x0D)
            {
                setTrackingStatus(TRACKING_STATE.Verify);
            }
            else
            {
                setTrackingStatus(TRACKING_STATE.Ready);
            }
        }
        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x0D)
            {
                setTrackingStatus(TRACKING_STATE.Verify);
                //e.Handled = false; ;
            }
            else
            {
                setTrackingStatus(TRACKING_STATE.Ready);
            }
        }
        private void checkBoxVerifying_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkBoxVerifying.Checked)
            {
                setTrackingStatus(TRACKING_STATE.Verify);
            }
            else
            {
                setTrackingStatus(TRACKING_STATE.Ready);
            }
        }
        private void setTrackingStatus(TRACKING_STATE state)
        {
            string status = "";

            this.trackingState = state;

            switch (this.trackingState)
            {
                case TRACKING_STATE.Pass:
                    status = dataGridView1.Rows[this.selectedVerifyingRow].Cells["LABELNR"].Value.ToString().Trim();
                    panelStatus.BackColor = Color.Chartreuse;
                    break;
                case TRACKING_STATE.Verify:
                    status = "WAITING";
                    panelStatus.BackColor = Color.PowderBlue;
                    timerQuery.Enabled = true;
                    checkBoxVerifying.Checked = true;
                    this.selectedVerifyingRow = 0;
                    break;
                case TRACKING_STATE.Fail:
                    status = dataGridView1.Rows[this.selectedVerifyingRow].Cells["LABELNR"].Value.ToString().Trim();
                    panelStatus.BackColor = Color.Red;
                    break;
                case TRACKING_STATE.Ready:
                    status = "READY";
                    panelStatus.BackColor = Color.Olive;

                    timerQuery.Enabled = false;
                    checkBoxVerifying.Checked = false;
                    break;
                default:
                    break;
            }

            if (this.trackingState != this.trackingPreviousState)
            {
                this.trackingPreviousState = this.trackingState;
                this.trackingStateChangeTimeStamp = DateTime.Now;
            }
            labelStatus.Text = status;
        }

        private void numericUpDownWONR_ValueChanged(object sender, EventArgs e)
        {
            if (trackingState != TRACKING_STATE.Ready)
            {
                setTrackingStatus(TRACKING_STATE.Verify);
            }
        }
    }
}
