﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace GeoTagDataBase
{
    public partial class FormDB : Form
    {
        enum TRACKING_STATE
        {
            Ready,
            Verify,
            Pass,
            Fail
        }

        private const string GEO_CONN_STRING = "Data Source=aaebio\\bsserver;Initial Catalog=GeoRiotinto;User ID=rfid;Password=rfid";
        private const string SELECTED_COLUMN = "[WONR], [LABELNR], [VERIFIED]";
        private SqlDataAdapter dataAdapter = new SqlDataAdapter();
        private DataTable table;
        private TRACKING_STATE trackingState;
        private TRACKING_STATE trackingPreviousState;
        private DateTime trackingStateChangeTimeStamp;
        private int selectedVerifyingRowIdx;
        private string errorMessage = "";
        private string tagType = "";
        private Dictionary<string, string> tagTypeFormat;
        private bool f_msg_shown = false;

        public FormDB()
        {
            InitializeComponent();
            this.Text += " (" + Application.ProductVersion + ")";
        }

        private void FormDB_Load(object sender, EventArgs e)
        {
            table = new DataTable();
            this.sqlConnection1.ConnectionString = GEO_CONN_STRING;
            setTrackingStatus(TRACKING_STATE.Verify);
            this.numericUpDownWONR.Select();
            initTagTypeFormat();
            reloadParameter();
        }
        private void initTagTypeFormat()
        {
            tagTypeFormat = new Dictionary<string, string>();
            tagTypeFormat.Add("BHP", @"[A-Z]{1}\d{6}$");
            tagTypeFormat.Add("AAxxxxx", @"[A-Z]{2}\d{5}$");
            tagTypeFormat.Add("BNxxxxxx (6)", @"[A-Z]{2}\d{6}$");
            tagTypeFormat.Add("BNxxxxxxx (7)", @"[A-Z]{2}\d{7}$");
            tagTypeFormat.Add("Rio Tinto", @"[A-Z]{4}\d{3}$");
        }
        private void reloadParameter()
        {
            tagType = textBoxTagType.Text;
        }
        
        private void timerQuery_Tick(object sender, EventArgs e)
        {
            queryTagInfo();
        }
        private void queryTagInfo()
        {
            if (this.numericUpDownWONR.Value > 1000000)
            {
                string selectCommand = "SELECT " + SELECTED_COLUMN + " from TAGSREAD "
                    + "WHERE [PASS] = \'PASS\' AND [WONR] = \'"
                    + this.numericUpDownWONR.Value.ToString() + "\' ORDER BY [LABELNR]";
                GetData(selectCommand);
                if (this.trackingState != TRACKING_STATE.Ready)
                {
                    verifyTag();
                }

                labelRowCount.Text = "Row no. = " + dataGridView1.RowCount;
                labelNotVerifiedCount.Text = "Not verified no. = " + checkNotVerified();
                labelSelectedVerifyingIndex.Text = "Idx. " + this.selectedVerifyingRowIdx.ToString();
                //labelWritingSpeed.Text = "Write speed = " + calculateSpeed() + " tag/s";
                f_msg_shown = false;
            }
            else
            {
                if (f_msg_shown)
                    return;
                f_msg_shown = true;
                MessageBox.Show("Please type label number 8 characters", "Input Error");

                setTrackingStatus(TRACKING_STATE.Ready);
            }
        }
        private void GetData(string selectCommand)
        {
            try
            {
                // Specify a connection string. Replace the given value with a 
                // valid connection string for a Northwind SQL Server sample
                // database accessible to your system.
                //String connectionString = this.sqlConnection1.ConnectionString;
                String connectionString = GEO_CONN_STRING;

                // Create a new data adapter based on the specified query.
                dataAdapter = new SqlDataAdapter(selectCommand, connectionString);

                // Create a command builder to generate SQL update, insert, and
                // delete commands based on selectCommand. These are used to
                // update the database.
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                // Populate a new data table and bind it to the BindingSource.
                //table = new DataTable();
                table.Clear();
                //table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSourceGeoRiotinto.DataSource = table;
            }
            catch (SqlException ex)
            {
                setTrackingStatus(TRACKING_STATE.Ready);
                MessageBox.Show(ex.ToString(), "SQL Error");
            }
        }

        private void verifyTag()
        {
            if (dataGridView1.RowCount > this.selectedVerifyingRowIdx)
            {
                switch (this.trackingState)
                {
                    case TRACKING_STATE.Verify:
                    case TRACKING_STATE.Pass:
                        if (verifyTagForward() && verifyTagLabelFormat() && verifyJumpingNumber())
                        {
                            this.setTrackingStatus(TRACKING_STATE.Pass);
                        }
                        else
                        {
                            this.setTrackingStatus(TRACKING_STATE.Fail);
                        }
                        break;
                    case TRACKING_STATE.Fail:
                        if (verifyTagBackward())
                        {
                            this.setTrackingStatus(TRACKING_STATE.Pass);
                        }
                        break;
                    default:
                        break;
                }

                this.dataGridView1.CurrentCell = dataGridView1.Rows[this.selectedVerifyingRowIdx 
                    + getMaxOffsetRow(2)].Cells[0];
                this.dataGridView1.CurrentCell = dataGridView1.Rows[this.selectedVerifyingRowIdx].Cells[0];
            }
        }
        private bool verifyTagBackward()
        {
            for (int i = 0; i < this.selectedVerifyingRowIdx + 1; i++)
            {
                Object ob = dataGridView1.Rows[i].Cells["VERIFIED"].Value;
                int j = Convert.ToInt16(ob.ToString());
                if (j == 0)
                {
                    this.selectedVerifyingRowIdx = i;
                    this.errorMessage = "Idx. " + i + " has not verified yet";
                    return false;
                }
            }

            return true;
        }
        private bool verifyTagForward()
        {
            for (int i = this.selectedVerifyingRowIdx; i < this.dataGridView1.RowCount; i++)
            {
                Object ob = dataGridView1.Rows[i].Cells["VERIFIED"].Value;
                int j = Convert.ToInt16(ob.ToString());
                if (j == 0)
                {
                    for (int k = this.selectedVerifyingRowIdx + 1; k < this.dataGridView1.RowCount; k++)
                    {
                        ob = dataGridView1.Rows[k].Cells["VERIFIED"].Value;
                        j = Convert.ToInt16(ob.ToString());
                        if (j != 0)
                        { /* Next indexes have verified */
                            this.selectedVerifyingRowIdx = i;
                            this.errorMessage = "Idx. " + k + " has already been verified";
                            return false;
                        }
                    }

                    if (i > 0)
                    { /* Next indexes have not verified */
                        this.selectedVerifyingRowIdx = i - 1;
                    }
                    else
                    { /* There is no verified tag */
                    }
                    return true;
                }
                else
                { /* Next indexes have verified */
                    this.selectedVerifyingRowIdx = i;
                }
            }
            /* Verified all */
            this.selectedVerifyingRowIdx = this.dataGridView1.RowCount - 1;
            return true;
        }
        private bool verifyTagLabelFormat()
        {
            string sPattern = tagTypeFormat[tagType];
            string s = "";

            for (int i = 0; i < this.dataGridView1.RowCount; i++)
            {
                s = dataGridView1.Rows[i].Cells["LABELNR"].Value.ToString();
                s = s.Trim();
                if (!Regex.IsMatch(s, sPattern))
                {
                    this.errorMessage = "Idx. " + i + " has incorrect format";
                    return false;
                }
            }

            return true;
        }
        private bool verifyJumpingNumber()
        {
            string sPattern = tagTypeFormat[tagType];
            string s = "";
            int index = this.dataGridView1.RowCount - 1;
            int prevValue = Int32.Parse(Regex.Match(
                dataGridView1.Rows[index].Cells["LABELNR"].Value.ToString(), @"\d+").Value);
            int value = 0;
            int range = 250;

            if (this.dataGridView1.RowCount < range)
                range = this.dataGridView1.RowCount;
            if (range < 2)
                return true;

            for (int i = 1; i < range; i++)
            {
                s = dataGridView1.Rows[index - i].Cells["LABELNR"].Value.ToString();
                s = s.Trim();
                s = Regex.Match(s, @"\d+").Value;
                value = Int32.Parse(s);
                if (value + 1 != prevValue)
                {
                    this.errorMessage = "Idx. " + (index - i + 2).ToString() + " has jumping number";
                    return false;
                }
                prevValue = value;
            }

            return true;
        }
        private int getMaxOffsetRow(int _offsetRowNumber)
        {
            int mxOffset = (dataGridView1.RowCount - 1) - this.selectedVerifyingRowIdx;
            if (_offsetRowNumber > mxOffset) _offsetRowNumber = mxOffset;
            return _offsetRowNumber;
        }
        private string checkNotVerified()
        {
            int verifiedCount = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                Object ob = dataGridView1.Rows[i].Cells["VERIFIED"].Value;
                verifiedCount += Convert.ToInt16(ob.ToString());
            }
            return (dataGridView1.Rows.Count - verifiedCount).ToString();
        }
        //private string calculateSpeed()
        //{
        //    DateTime firstTagTime;
        //    DateTime lastTagTime;
        //    float writingSpeed = 0;

        //    for (int i = 0; i < this.dataGridView1.RowCount; i++)
        //    {
        //        Object ob1 = dataGridView1.Rows[i].Cells["LABELNR"].Value;
        //        Object ob2 = dataGridView1.Rows[i].Cells["VERIFIED"].Value;

        //        string s = ob1.ToString();

        //        //if (!System.Text.RegularExpressions.Regex.IsMatch(s, sPattern))
        //        {
        //            //this.errorMessage = "Idx. " + i + " has incorrect format";
        //            return "";
        //        }
        //    }
        //    return "";
        //}

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            //setTrackingStatus(TRACKING_STATE.Ready);
        }
        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            setTrackingStatus(TRACKING_STATE.Ready);
            this.selectedVerifyingRowIdx = GetRowIndexOfDataGridView1();
        }
        private int GetRowIndexOfDataGridView1()
        {
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (dataGridView1.Rows[i].Selected)
                {
                    return i;
                }
            }
            return dataGridView1.RowCount;
        }

        private void buttonExecuteQuery_Click(object sender, EventArgs e)
        {
            setTrackingStatus(TRACKING_STATE.Verify);
        }
        private void numericUpDownWONR_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x0D)
            {
                setTrackingStatus(TRACKING_STATE.Verify);
            }
            else
            {
                setTrackingStatus(TRACKING_STATE.Ready);
            }
        }
        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x0D)
            {
                setTrackingStatus(TRACKING_STATE.Verify);
                //e.Handled = false; ;
            }
            else
            {
                setTrackingStatus(TRACKING_STATE.Ready);
            }
        }
        private void checkBoxVerifying_CheckStateChanged(object sender, EventArgs e)
        {
            //if (checkBoxVerifying.Checked)
            //{
            //    setTrackingStatus(TRACKING_STATE.Verify);
            //}
            //else
            //{
            //    setTrackingStatus(TRACKING_STATE.Ready);
            //}
        }
        private void setTrackingStatus(TRACKING_STATE state)
        {
            string status = "";

            this.trackingState = state;
            switch (this.trackingState)
            {
                case TRACKING_STATE.Verify:
                    status = "WAITING";
                    panelStatus.BackColor = Color.PowderBlue;
                    this.errorMessage = "";
                    timerQuery.Enabled = true;
                    checkBoxVerifying.Checked = true;
                    this.selectedVerifyingRowIdx = 0;
                    break;
                case TRACKING_STATE.Pass:
                    status = dataGridView1.Rows[this.selectedVerifyingRowIdx].Cells["LABELNR"].Value.ToString().Trim();
                    panelStatus.BackColor = Color.Chartreuse;
                    this.errorMessage = "";
                    break;
                case TRACKING_STATE.Fail:
                    status = dataGridView1.Rows[this.selectedVerifyingRowIdx].Cells["LABELNR"].Value.ToString().Trim();
                    panelStatus.BackColor = Color.Red;
                    break;
                case TRACKING_STATE.Ready:
                    status = "READY";
                    panelStatus.BackColor = Color.Olive;

                    timerQuery.Enabled = false;
                    checkBoxVerifying.Checked = false;
                    break;
                default:
                    break;
            }

            if (this.trackingState != this.trackingPreviousState)
            {
                this.trackingPreviousState = this.trackingState;
                this.trackingStateChangeTimeStamp = DateTime.Now;
            }
            labelStatus.Text = status;
            labelError.Text = this.errorMessage;
        }

        private void numericUpDownWONR_ValueChanged(object sender, EventArgs e)
        {
            if (trackingState != TRACKING_STATE.Ready)
            {
                setTrackingStatus(TRACKING_STATE.Verify);
            }

            global::GeoTagDataBaseMonitor.Properties.Settings.Default.Save();
        }

        private void buttonSetup_Click(object sender, EventArgs e)
        {
            setParameter();
        }
        private void setParameter()
        {
            FormSetParameter formSetParameter;
            DialogResult dialogResult;

            formSetParameter = new FormSetParameter();
            dialogResult = formSetParameter.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                reloadParameter();
            }
        }
    }
}
