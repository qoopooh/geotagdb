﻿namespace GeoTagDataBase
{
    partial class FormSetParameter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSetParameter));
            this.buttonSettingOk = new System.Windows.Forms.Button();
            this.buttonSettingCancel = new System.Windows.Forms.Button();
            this.buttonDefaultSetting = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxTagType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // buttonSettingOk
            // 
            this.buttonSettingOk.Location = new System.Drawing.Point(108, 78);
            this.buttonSettingOk.Name = "buttonSettingOk";
            this.buttonSettingOk.Size = new System.Drawing.Size(90, 23);
            this.buttonSettingOk.TabIndex = 6;
            this.buttonSettingOk.Text = "&OK";
            this.buttonSettingOk.UseVisualStyleBackColor = true;
            this.buttonSettingOk.Click += new System.EventHandler(this.buttonSettingOk_Click);
            // 
            // buttonSettingCancel
            // 
            this.buttonSettingCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonSettingCancel.Location = new System.Drawing.Point(204, 78);
            this.buttonSettingCancel.Name = "buttonSettingCancel";
            this.buttonSettingCancel.Size = new System.Drawing.Size(90, 23);
            this.buttonSettingCancel.TabIndex = 7;
            this.buttonSettingCancel.Text = "&Cancel";
            this.buttonSettingCancel.UseVisualStyleBackColor = true;
            this.buttonSettingCancel.Click += new System.EventHandler(this.buttonSettingCancel_Click);
            // 
            // buttonDefaultSetting
            // 
            this.buttonDefaultSetting.Location = new System.Drawing.Point(12, 78);
            this.buttonDefaultSetting.Name = "buttonDefaultSetting";
            this.buttonDefaultSetting.Size = new System.Drawing.Size(90, 23);
            this.buttonDefaultSetting.TabIndex = 5;
            this.buttonDefaultSetting.Text = "&Default Setting";
            this.buttonDefaultSetting.UseVisualStyleBackColor = true;
            this.buttonDefaultSetting.Click += new System.EventHandler(this.buttonDefaultSetting_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Tag type:";
            // 
            // comboBoxTagType
            // 
            this.comboBoxTagType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxTagType.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::GeoTagDataBaseMonitor.Properties.Settings.Default, "TagType", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.comboBoxTagType.FormattingEnabled = true;
            this.comboBoxTagType.Items.AddRange(new object[] {
            "AAxxxxx",
            "BNxxxxxx (6)",
            "BNxxxxxxx (7)",
            "BHP",
            "Rio Tinto"});
            this.comboBoxTagType.Location = new System.Drawing.Point(70, 6);
            this.comboBoxTagType.Name = "comboBoxTagType";
            this.comboBoxTagType.Size = new System.Drawing.Size(131, 21);
            this.comboBoxTagType.TabIndex = 9;
            this.comboBoxTagType.Text = global::GeoTagDataBaseMonitor.Properties.Settings.Default.TagType;
            // 
            // FormSetParameter
            // 
            this.AcceptButton = this.buttonSettingOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonSettingCancel;
            this.ClientSize = new System.Drawing.Size(307, 111);
            this.Controls.Add(this.comboBoxTagType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonDefaultSetting);
            this.Controls.Add(this.buttonSettingCancel);
            this.Controls.Add(this.buttonSettingOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormSetParameter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Set Parameter";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormSetParameter_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSettingOk;
        private System.Windows.Forms.Button buttonSettingCancel;
        private System.Windows.Forms.Button buttonDefaultSetting;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTagType;
    }
}
