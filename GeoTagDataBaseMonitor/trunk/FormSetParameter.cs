﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace GeoTagDataBase
{
    public partial class FormSetParameter : Form
    {
        public FormSetParameter()
        {
            InitializeComponent();
        }

        private void FormSetParameter_Load(object sender, EventArgs e)
        {
        }

        private void buttonSettingCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            global::GeoTagDataBaseMonitor.Properties.Settings.Default.Reload();
            this.Close();
        }

        private void buttonSettingOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            global::GeoTagDataBaseMonitor.Properties.Settings.Default.Save();
            this.Close();
        }

        private void buttonDefaultSetting_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to reset all settings?", 
                "Confirm reset", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                this.DialogResult = DialogResult.OK;
                global::GeoTagDataBaseMonitor.Properties.Settings.Default.Reset();
                this.Close();
            }
        }
    }
}
