# GeoTagDB #
These softwares are written in C# to manage / monitor MSSQL database server.
Please kindly see latest revisions on [Google Drive](https://drive.google.com/folderview?id=0Bx2JArkhdsXcfngyMUh1NWVNUm52UUMwYlFOdTktU0FJalFPZnloLV9WSTBobUcxNnE4ZUU&usp=sharing).

To open the projects, please go to *GeoTagDataBase/GeoTagDataBase.sln*. The sources are in trunk folder (keep legacy style from svn)

## Create MSSQL Server ##
GeoTagDB work with specific MSSQL database server, you might have to specify instance name as well.

### Requirement ###
* [SQLEXPR](https://drive.google.com/open?id=0Bx2JArkhdsXcTk1kcG9JcDhTT1U) - MSSQL Server 2005
* [SQLServer2005_SSMSEE_x64](https://drive.google.com/open?id=0Bx2JArkhdsXcUjhxQ25MMU5oRVE) - SQL Manager
* Backup of database

## Server Installation Procedure ##
In case you lost the server again.

### Install SQLEXPR
* Set instance name to bsserver
* Set sa password to be complex ( we will change it back later )

### Install SQLServer2005_SSMSEE_x64
* Log in to SQL Management with sa
* Go to **aaebio\bsserver** -> Security -> Logins -> sa then change the password. Do not forget to uncheck *Enforce password policy*
* Logout / Login sa again

### Attach GeoRiotinto database
* Right click on Databases to attach GeoRiotinto.mdf
* Go to aaebio\bsserver -> Security -> Logins to create new login **rfid**
* Set password to rfid
* Set Default database to GeoRiotinto
* Check Server Roles to sysadmin
* Re-login with **rfid**
